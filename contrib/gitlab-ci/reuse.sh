#!/bin/bash
#
# Lints the repository before running anything else

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

. $( dirname $0 )/cilib.sh

# The reuse tool does not have useful exit codes,
# so we need to search the output and turn that into
# an exit code. Typical output is:
#
#    Unfortunately, your project is not compliant with version 3.0 of the REUSE Specification :-(
#
# or
#
#    Congratulations! Your project is compliant with version 3.0 of the REUSE Specification :-)
#
# Display the last 13 lines, which is the summary information.

reuse_lint_log() {
	LOG=reuse.txt
	tempLOG=/tmp/reuse.$$.log.tmp
	rm -f ctest.html
	reuse lint > $LOG 2>&1 
	tail -13 $LOG | tee $tempLOG
	grep -q '^Congratulations' $tempLOG > /dev/null
	R=$?
	return $R
}

cmd_check reuse_lint_log

