#! /bin/sh
#
# Definitions for colorized output, command-logging, etc.
# Source this file from CI scripts.

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

### COLOR DEFINITIONS
#
#
ESC=$(printf '\033')
CLR_RED="$ESC[31;1m"
CLR_GREEN="$ESC[32;1m"
CLR_YELLOW="$ESC[33m"
CLR_RESET="$ESC[0m"

### COLORIZED MESSAGES
#
#
warn() {
	echo ""
	echo "$CLR_RED""$@""$CLR_RESET"
}

info() {
	echo ""
	echo "$CLR_YELLOW""$@""$CLR_RESET"
}

### COMMAND LOGGING
#
# Commands are printed as they are executed, with something
# like a "shell environment" around them. Use cmd_ignore
# to ignore the command's exit-status, or cmd_check to
# bail out on a failure.
#
# For convenience, cmd is an alias of cmd_check (e.g. commands
# are checked by default)
#

maybe_exit() {
	EXITVAL=${1:-$?}
	if [ $EXITVAL -ne 0 ]
	then
		warn Error exit value $EXITVAL
		exit $EXITVAL
	fi
}

cmd_ignore() {
	echo '$ '"$CLR_GREEN""$@""$CLR_RESET"
	"$@"
}

cmd_check() {
	echo '# (dir is '"$CLR_YELLOW"$(pwd)"$CLR_RESET"')'
	echo '$ '"$CLR_GREEN""$@""$CLR_RESET"
	"$@"
	maybe_exit
}

cmd() {
	cmd_check "$@"
}

### BUILD SHORTCUTS
#
#
# Generally, use the cmake_*_self() functions, which build **this**
# repository consistently in a directory `cbuild/`, and use
# arpa2_get_dependency() to fetch already-built-artifacts from
# elsewhere in GitLab CI.

# Builds a dependency that is checked-out under `/builds/arpa2`.
# Use this only if there is no suitable arpa2_get_dependency()
# approach to getting that dependency.
cmake_build_dependency() {
	PKG="$1"
	DIR="/builds/arpa2/$PKG"
	cmd mkdir -p "$DIR"
	pushd "$DIR"
	info Building CMake project $PKG
	cmd mkdir cbuild
	cmd cd cbuild
	cmd cmake -D DEBUG:BOOL=OFF ..
	cmd make
	cmd make install
	popd
}

cmake_build_self() {
	info Configuring...
	cmd mkdir -p cbuild
	pushd cbuild
	cmd cmake -D DEBUG:BOOL=ON ..
	info Building...
	cmd make
	popd
}

cmake_package_self() {
	info Installing...
	cmd pushd cbuild
	cmd make install DESTDIR=dist
	cmd tar czf ../dist.tgz -C dist .
	popd
}

cmake_test_self() {
	info Testing
	cmd pushd cbuild
	cmd ctest -V --output-on-failure --output-log ../ctest.log
	popd
}

# Fetches a dependency produced by a GitLab pipeline.
#
# - $1 is the name of the repo / project to get the tarball from
# - $2 (optional) is the name of the **branch** in that repo, defaults
#      to master.
#
# With the conventional setup of ARPA2 GitLab pipelines, each repo
# calls cmake_package_self() from `build.sh` and uploads the resulting
# tarball as a build artifact. Use this function to download-and-install
# such a conventional tarball -- this works because all our builds happen
# on the same base VM.
#
arpa2_get_dependency() {
	info Fetching CI artifacts for "$1"
	BRANCH="${2:-master}"
	curl -o "$1.tgz" -L https://gitlab.com/arpa2/"$1"/-/jobs/artifacts/"$BRANCH"/raw/dist.tgz?job=build_n_test
	tar xf "$1.tgz" -C /
}
