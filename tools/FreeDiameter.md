# Pypeline for FreeDiameter

The FreeDiameter daemon `freeDiameterd` takes configuration files and it is
not trivial to find the point where it can enter freerunning mode.  The
examples below are from our project
[KIP](https://gitlab.com/arpa2/kip/),
which runs a client and a server.

<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
-->


## Old Method

We used to have a wrapper, namely
[wrap_diameterd](https://gitlab.com/arpa2/pypeline/-/raw/master/tools/wrap_diameterd),
which we would prefix the command as in

```
${CMAKE_CURRENT_SOURCE_DIR}/wrap_diameterd SIG:INT "Core state: 2-> 3" ${freeDiameter_EXECUTABLE} ...
```

for the server and for the client we would use

```
${CMAKE_CURRENT_SOURCE_DIR}/wrap_diameterd SIG:INT "'STATE_OPEN'" ${freeDiameter_EXECUTABLE} ...
```

The main task of the wrapper was to recognise fixed patterns in the input.  This was necessary
because the whole lines did not have a fixed match.  With the introduction of regular expressions
in `PYPE:FORK` this could finally move into Pypeline itself and the wrapper is no longer used.


## Matching Patterns

On the client, we now use

```
SIG:TERM PYPE:NAME:freediameter-client "PYPE:FORK:.*STATE_OPEN.*"       ${freeDiameter_EXECUTABLE} ...
```

On the server, we now use

```
SIG:TERM PYPE:NAME:freediameter-server "PYPE:FORK:.*Core.state:.2.*3.*" ${freeDiameter_EXECUTABLE} ...
```

The `.` and `.*` are slightly inaccurate compared to a literal match, but the initiation code
is not a problem in that sense; all output is local.


## Configuration Files

*The following recipe repeats for (kinds of) client and (kinds of) server.*

We usually prepare a `freediameter.conf.in` configuration file with contents like

```
## -------- Test configuration ---------

Identity = "fdsrv.unicorn.demo.arpa2.org";
Realm = "unicorn.demo.arpa2.org";
ListenOn = "#IP:DIAMETER#";
SecPort = #SCTP:DIAMETER#;
Port = 0;
No_TCP;

TLS_Cred = "@CMAKE_BINARY_DIR@/test/fdsrv.cert.pem",
           "@CMAKE_BINARY_DIR@/test/fdsrv.key.pem";
TLS_CA = "@CMAKE_BINARY_DIR@/test/cacert.pem";
```

These first pass through CMake and then have a name like `freediameter.conf`.

The `#IP:DIAMETER#` and `SCTP:DIAMETER#` variables remain and will be filled by Pypeline,
when called like this:

```
... ${freeDiameter_EXECUTABLE} -ddc INFILE:DIACONF:${CMAKE_CURRENT_BINARY_DIR}/freediameter.conf`
```

Now, the variables must be mentioned in the Pypeline script, and one way of doing that is
before the actual command, where they produce no output words but end up being declared:

```
#IP:DIAMETER# #SCTP:DIAMETER#
... ${freeDiameter_EXECUTABLE} -ddc INFILE:DIACONF:${CMAKE_CURRENT_BINARY_DIR}/freediameter.conf`
```


## Adding an Extension

To load an extension with its own configuration file, things get more complicated.
We must now map an extension configuration `diameter-ext.conf.in` and, if that too
holds variables like `#TCP:DIAEXT#` and `#IP:DIAEXT#` below, then they must also
be mapped by Pypeline, perhaps after `configure_file()` by CMake to setup build-env
variables; plus, the produced file must be referenced from the master configfile.

We use yet another prefix to the actual command, namely `INFILE:` as in

```
#IP:DIAMETER# #SCTP:DIAMETER#
#IP:DIAEXT#   #TCP:DIAEXT#
INFILE:DIAEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diameter-ext.conf
... ${freeDiameter_EXECUTABLE} -ddc INFILE:DIACONF:${CMAKE_CURRENT_BINARY_DIR}/freediameter.conf`
```

The latter file now has an extra line to load the extension, whose dynamic name
it inserts as `FILE:DIAEXTCONF` thusly:

```
LoadExtension = "@CMAKE_BINARY_DIR@/src/diaext.fdx" : "#FILE:DIAEXTCONF#";
```

## Setting Macros

This all becomes a bit silly, so it is advised to use macros.  Here is an
example from KIP with one client type and two kinds of server:

```
set (freediameter-client
    INFILE:DIACLIEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-client.conf
    SIG:TERM PYPE:NAME:freediameter-client "PYPE:FORK:.*STATE_OPEN.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIACLICONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-client.conf)

set (freediameter-server
    INFILE:DIASRVEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-server.conf
    SIG:TERM PYPE:NAME:freediameter-server "PYPE:FORK:.*Core.state:.2.*3.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIASRVCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server.conf)

set (freediameter-server-haan
    INFILE:DIASRVEXTCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-extension-server-haan.conf
    SIG:TERM PYPE:NAME:freediameter-server "PYPE:FORK:.*Core.state:.2.*3.*"
    ${freeDiameter_EXECUTABLE} -ddc INFILE:DIASRVCONF:${CMAKE_CURRENT_BINARY_DIR}/bin/diasasl-server.conf)
```

This can easily be used in tests like

```
add_test (t_haan_generate_diasasl ${pypeline} ${setall} ${sethaanplugin}
    IP:DIASASL  TCP:DIASASL
    IP:DIAMETER SCTP:DIAMETER
    ${hsasl-makeid} IP:HERE TCP:SASL
    --
    ${diasasl-proxy-synchr} IP:DIASASL TCP:DIASASL IP:HERE TCP:SASL
    --
    ${freediameter-client}
    --
    ${freediameter-server-haan}
)
```

It's a bit of a struggle, but once setup it will help you test FreeDiameter reliably!
