# Pypeline for Apache2

Testing with the Apache2 web server requires the patience until it listens
to its HTTP ports.  This can be achieved with a special
[Pypeline Signaling module](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_pypeline_signal)
that prints the special sequence `"--\n"` on Apache's `stdout` stream,
just as Pypeline expects by default.

<!--
SPDX-License-Identifier: BSD-2-Clause
SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
-->

To use it, add this line to your `apache2.conf` file:

```
LoadModule arpa2_pypeline_signal_module path/to/mod_arpa2_pypeline_signal.so
```

**Links:**

  * [Apache ARPA2 Modules repository](https://gitlab.com/arpa2/apachemod)
  * [Apache tests with Pypeline in CMakeLists.txt](https://gitlab.com/arpa2/apachemod/-/blob/master/test/CMakeLists.txt)
  * [Apache ARPA2 Pypeline Signaling module](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_pypeline_signal)
