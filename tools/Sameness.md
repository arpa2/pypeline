# Sameness between outputs

Compile `sameness.c` for a tool that helps to classify strings.
Use Pypeline to produce those strings, for instance using `FILE:xxx`
to generate files, possibly with the help of `PYPE:POPCAT:n` to
stack them together.

Using `sameness` on the results, you can use abstract classifiers:

  * `-A` through `-Z` are used to enforce differences when the
    letters differ.  So, a class `-A` may internally be the same
    or not, but anything in it must be different from anything in
    class `-B`, `-C`, and so on.  Likewise between `-B` and `-C`.

  * `-a` through `-z` are used to enforce sameness when the
    letters are the same.  So, a class `-a` must internally be
    the same, as is `-b`, and so on.  Comparisons between `-a`
    and `-b` are not made, also not to enforce difference.

  * `-1` through `-9` require _some_ overlap in values.  For
    instance, if there are 3 values in class `-1` then there
    may not be more than 3-1=2 different values in it.  This
    is used to test for repetition, especially when it depends
    on time and strict difference of all values in the class
    would be too strong a requirement.  Such runs could compute
    3 values in fast succession and demand at most 2 values.
    Different classes such as `-1` and `-2` are not compared
    at all.

  * You might use `-Aa` and -Bb` as combined classes, which then
    demands the same value in equal classes and different values
    in different classes.  This is often what you want, but in a
    testing environment the subtler split can be quite useful.

The origin of this tool is the
[ARPA2 Tartaros](https://gitlab.com/arpa2/tartaros)
project, where it was used to check that `REMOTE_USER_TOKEN`
output varied for different `ARPA2_DOMAIN`, `ARPA2_REMOTE_USER`,
`ARPA2_SERVICE_CONTRACT` but that the values generated within
one second were the same.
