/* sameness.c  --  Compare arguments in a set to be the same or different
 *
 * Very often, a set of test outputs must be different.
 * Other times, they need to be the same.
 * This utility exhaustively goes over such checks.
 * It aims to be thorough on small files, not efficient on large.
 *
 * Since some values depend on time, there is an argument to indicate a
 * maximum time range between files in a range; if it is larger, a hint
 * is made to re-run the test or improve its speed.  This is not an
 * error.  The argument is "@TTT.TTT" where "TTT.TTT" is a number of
 * seconds.  Without this argument, the time range is not considered.
 *
 * The output is silent if all is well, but is explanatory otherwise.
 * It uses a description ":UNIT" of the kind of objects in the set, and
 * likes all files FFF prefixed with a (short) label LLL, as in "LLL=FFF",
 * so it can say things like 'John case 1a ought to differ from Mary 2b".
 * Just saying "=FFF" will reuse the previous label "LLL".
 *
 * The numeric classes -1, -2 through -9 distinguish similar cases
 * that must have _some_ overlap over the time range.  Without time
 * range they must all be the same otherwise.
 *
 * Lowercase letter options -a, -b through -z classify values that
 * must be the same when the letter is the same.  This makes lowercase
 * letters a dimension for sameness.  Each lowercase letter replaces
 * a previously set lowercase letter.
 *
 * Uppercase letter options -A, -B through -Z classify values that
 * must be different when the letter differs.  This makes uppercase
 * letters a dimension for difference.  Each uppercase letter replaces
 * a previously set uppercase letteroptions.
 *
 * You may combine uppercase and lowercase letters as in -aA -bB -cC
 * to have a strict separation where the values are the same when they
 * all have the same letter pair, and different when they have a
 * different letter pair.
 *
 * You are not forced to use all classifications.  You may reset all
 * previous classifications with letters and digits with -0.
 *
 * In summary:
 *  -1 -2 -3  means  "same number  =>  some overlap in values"
 *  -a -b -c  means  "same letter  =>  same value"
 *  -A -B -C  means  "diff letter  =>  diff value"
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>


struct casefile {
	//
	// The argument's label (up to '=')
	char *label;
	//
	// The argument's filename
	char *filename;
	//
	// Classes -1..-9, -a..-z, -A..-Z
	// Each can be '0' for no class
	char class_19;
	char class_az;
	char class_AZ;
	//
	// File description
	struct stat stat;
	//
	// The file contents being compared, and its length
	char *data;
	unsigned datalen;
	//
	// Dump this file at the end
	bool dump;
};




int main (int argc, char *argv[]) {
	//
	// Set initial values
	float range = INFINITY;
	char class_19 = '0', class_az = '0', class_AZ = '0';
	char *unit = NULL;
	//
	// Parse arguments, scan for stxerr and help flags, and collect case args
	bool help = false;
	bool stxerr = false;
	unsigned casec = 0;
	struct casefile casev [argc];
	memset (casev, 0, sizeof (casev));
	for (int argi = 1; argi < argc; argi++) {
		char *args = argv[argi];
		//
		// Parse the "@TTT.TTT" argument
		if (*args == '@') {
			if (range != INFINITY) {
				fprintf (stderr, "You cannot set -@ more than once\n");
				stxerr = true;
				continue;
			}
			char *endptr = NULL;
			range = strtof (args+1, &endptr);
			if ((endptr == args+1) || (*endptr != '\0')) {
				fprintf (stderr, "Trailing garbage: @TTT.TTT%s\n", endptr);
				stxerr = true;
				continue;
			}
			fprintf (stderr, "Not currently implemented: %s\n", args);
			help = true;
		//
		// Parse the ":DESCR" argument
		} else if (*args == ':') {
			if (unit != NULL) {
				fprintf (stderr, "You cannot set :UNIT more than once\n");
				stxerr = true;
				continue;
			}
			unit = args+1;
		//
		// Parse classes -0, -1..-9, -a..-z, -A..-Z, -?
		} else if (*args == '-') {
			for (int opti = 1; args[opti] != '\0'; opti++) {
				char optc = args[opti];
				if (optc == '?') {
					help = true;
				} else if (optc == '0') {
					class_19 = class_az = class_AZ = '0';
				} else if ((optc >= '1') && (optc <= '9')) {
					class_19 = optc;
				} else if ((optc >= 'a') && (optc <= 'z')) {
					class_az = optc;
				} else if ((optc >= 'A') && (optc <= 'Z')) {
					class_AZ = optc;
				} else {
					fprintf (stderr, "Unknown option -%c given\n", optc);
					stxerr = true;
					continue;
				}
			}
		//
		// Handle a [[LABEL]=]FILENAME argument or raise an error
		} else {
			//
			// Collect the label
			unsigned labellen = strcspn (args, "=");
			char *label = NULL;
			char *file  = NULL;
			if (args [labellen] == '\0') {
				//
				// No '=' found, filename doubles as label
				label = file = args;
			} else if (labellen == 0) {
				//
				// String starts with '=', filename doubles as label
				// (may be useful to allow a filename containing '=')
				label = file = args + 1;
			} else if (args [labellen] == '=') {
				//
				// Found an inner '=', so split the string
				args [labellen] = '\0';
				label = args;
				file  = args + labellen + 1;
			} else {
				//
				// Unanticipated case, maybe misunderstood strcspn()
				fprintf (stderr, "Internal error triggered by %s\n", args);
				help = true;
				continue;
			}
			//
			// Possibly strip the directory from the label if it is a filename
			if (label == file) {
				char *slash = strrchr (label, '/');
				if ((slash != NULL) && (slash [1] != '\0')) {
					label = slash + 1;
				}
			}
			//
			// Be certain to have received a filename
			if (*file == '\0') {
				fprintf (stderr, "You did not include a file name: %s=\n", args);
				stxerr = true;
				continue;
			}
			// Store the case and count it
			casev[casec].label    = label;
			casev[casec].filename = file;
			casev[casec].class_19 = class_19;
			casev[casec].class_az = class_az;
			casev[casec].class_AZ = class_AZ;
			casec++;
		}
	}
	//
	// Set any lingering defaults
	if (unit == NULL) {
		unit = "Case";
	}
	//
	// Test if enough cases were brought in
	if (casec == 0) {
		help = true;
	} else if (casec < 2) {
		fprintf (stderr, "You need at least 2 cases to be able to compare them\n");
		stxerr = true;
	}
	//
	// Report an error, if any
	if (help || stxerr) {
		fprintf (stderr, "Usage: %s [@timevar] [:unit] -opts|[[label]=]file...\n"
			"where timevar sets a floating-point time varation range for -1..-9 (NOT IMPL)\n"
			"      unit    describes the kind of units being compared\n"
			"      label   is a short meaningful description of a case file\n"
			"      file    is a case file with the output to compare\n"
			"      opts    are dash-prefixed options:\n"
			"              -1..-9 so that same digit  requires some overlap in case files\n"
			"              -a..-z so that same letter requires same value    in case files\n"
			"              -A..-Z so that diff letter requires diff value    in case files\n"
			"              -0 to reset all classes -1..-9, -a..-z, -A..-Z\n"
			, argv [0]);
		exit (stxerr ? 1 : 0);
	}

	//
	// Load the case files into memory
	bool ok = true;
	for (int casei = 0; casei < casec; casei++) {
		struct casefile *casef = &casev[casei];
		//
		// Open the file
		int fd = open (casef->filename, O_RDONLY);
		if (fd < 0) {
			perror ("FATAL: Case file did not open for reading");
			fprintf (stderr, "FATAL: Error with case file %s\n", casef->filename);
			exit (1);
		}
		//
		// Obtain fstat() information
		if (fstat (fd, &casef->stat) != 0) {
			perror ("FATAL: File stats unavailable");
			fprintf (stderr, "FATAL: Error with case file %s\n", casef->filename);
			exit (1);
		}
		//
		// Allocate data memory
		casef->datalen = casef->stat.st_size;
		casef->data = malloc (casef->datalen);
		if (casef->data == NULL) {
			fprintf (stderr, "FATAL: Out of memory\n");
			exit (1);
		}
		//
		// Read the case file data
		ssize_t gotten = read (fd, casef->data, casef->datalen);
		if (gotten < 0) {
			perror ("Error reading data");
			fprintf (stderr, "FATAL: Error with case file %s\n", casef->filename);
			exit (1);
		} else if (gotten != casef->datalen) {
			fprintf (stderr, "FATAL: Got %zd not %zd from case file %s\n",
						gotten, casef->datalen, casef->filename);
			exit (1);
		}
		//
		// Close the case file
		close (fd);
		fd = -1;
	}

	//
	// Compare the cases -a..-z on a one-by-one basis
	// same letter  ==>  same data
	unsigned errors_az = 0;
	//
	// Iterate over "a" cases
	for (int a = 0 ; a < casec-1; a++) {
		struct casefile *af = &casev[a];
		//
		// Skip if "a" was not classified
		if (af->class_az == '0') {
			continue;
		}
		//
		// Iterate over "b" cases (located after "a")
		for (int b = a + 1; b < casec; b++) {
			struct casefile *bf = &casev[b];
			//
			// Skip if "b" was not classified
			if (bf->class_az == '0') {
				continue;
			}
			//
			// Skip if "a" and "b" have different classes
			if (af->class_az != bf->class_az) {
				continue;
			}
			//
			// Welcome the same data sizes and same data
			if ((af->datalen == bf->datalen) &&
			    (0 == memcmp (af->data, bf->data, af->datalen))) {
				continue;
			}
			//
			// Complain that the data should have matched
			fprintf (stderr, "ERROR: %ss -%c %s and -%c %s should have been the same\n",
					unit,
					af->class_az, af->label,
					bf->class_az, bf->label);
			af->dump = bf->dump = true;
			errors_az++;
		}
	}

	//
	// Compare the cases -A..-Z on a one-by-one basis
	// different letter  ==>  different data
	unsigned errors_AZ = 0;
	//
	// Iterate over "a" cases
	for (int a = 0 ; a < casec-1; a++) {
		struct casefile *af = &casev[a];
		//
		// Skip if "a" was not classified
		if (af->class_AZ == '0') {
			continue;
		}
		//
		// Iterate over "b" cases (located after "a")
		for (int b = a + 1; b < casec; b++) {
			struct casefile *bf = &casev[b];
			//
			// Skip if "b" was not classified
			if (bf->class_AZ == '0') {
				continue;
			}
			//
			// Skip if "a" and "b" have the same class
			if (af->class_AZ == bf->class_AZ) {
				continue;
			}
			//
			// Welcome different data sizes
			if (af->datalen != bf->datalen) {
				continue;
			}
			//
			// Welcome different data bytes
			if (0 != memcmp (af->data, bf->data, af->datalen)) {
				continue;
			}
			//
			// Complain that the data should not match
			fprintf (stderr, "ERROR: %ss -%c %s and -%c %s should have been different\n",
					unit,
					af->class_AZ, af->label,
					bf->class_AZ, bf->label);
			af->dump = bf->dump = true;
			errors_AZ++;
		}
	}

	//
	// Compare the cases -1..-9 on a one-by-one basis
	// same digit  ==>  some overlap requried (unless time range is permissive)
	unsigned found_19 [10];
	unsigned match_19 [10];
	memset (found_19, 0, sizeof (found_19));
	memset (match_19, 0, sizeof (match_19));
	for (int a = 0; a < casec; a++) {
		struct casefile *af = &casev[a];
		//
		// Skip if "a" was not classified
		if (af->class_19 == '0') {
			continue;
		}
		//
		// Signal that we found a value for this digit
		int index = af->class_19 - '0';
		found_19[index]++;
		//
		// Iterate over "b" cases (located after "a")
		for (int b =  a + 1; b < casec; b++) {
			struct casefile *bf = &casev[b];
			//
			// Skip if "b" was not classified
			if (bf->class_19 == '0') {
				continue;
			}
			//
			// Skip if "a" and "b" have different classes
			if (af->class_19 != bf->class_19) {
				continue;
			}
			//
			// Hope to find a match for this digit
			if ((af->datalen == bf->datalen) &&
			    (0 == memcmp (af->data, bf->data, af->datalen))) {
				match_19[index]++;
				//
				// Only count the first of multiple matches
				break;
			}
		}
	}
	//
	// Iterate over -1..-9 and report when found without match
	unsigned errors_19 = 0;
	for (int index = 1; index <= 9; index++) {
		char digit = '0' + index;
		if ((found_19[index] > 0) && (match_19[index] == 0)) {
			/* We only report this general statement in the error case: */
			fprintf (stderr, "ERROR: %ss -%c occured %d times with %d values in",
					unit, digit, found_19[index], found_19[index]-match_19[index]);
			char *comma = ": ";
			for (int casei = 0; casei < casec; casei++) {
				if (casev[casei].class_19 == digit) {
					fprintf (stderr, "%s%s", comma, casev[casei].label);
					comma = ", ";
					casev[casei].dump = true;
				}
			}
			fprintf (stderr, "\n");
			errors_19++;
		}
	}

	//
	// Dump the files that showed up in an error
	for (int casei = 0; casei < casec; casei++) {
		struct casefile *casef = &casev[casei];
		//
		// Skip case files that don't need dumping
		if (!casef->dump) {
			continue;
		}
		//
		// Print a clear file header
		printf ("-----\n----- DUMPING FILE WITH LABEL %s\n-----\n", casef->label);
		//
		// Print the filee
		printf ("%.*s", casef->datalen, casef->data);
	}

	// Cleanup dynamic memory
	for (int casei = 0; casei < casec; casei++) {
		free (casev[casei].data);
		casev[casei].data = NULL;
		casev[casei].datalen = 0;
	}

	//
	// Report overall errors
	unsigned errors = errors_az + errors_AZ + errors_19;
	if (errors > 0) {
		fprintf (stderr, "\nSUMMARY: Total errors: %d, not equal: %d, not different: %d, not overlapped: %d\n",
				errors, errors_az, errors_AZ, errors_19);
	}
	//
	// Return 0 or exit because of the error code
	if (errors > 0) {
		exit (1);
	}
	return 0;
}
